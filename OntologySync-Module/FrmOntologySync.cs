﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.Threading;
using System.Windows.Forms;
using OntologyAppDBConnector;
using OntologySync_Module.OntoWeb;
using Ontology_Module;
using OntoMsg_Module;
using Security_Module;
using Structure_Module;
using System.Runtime.InteropServices;
using WebSocketSharp;
using WebSocketSharp.Server;
using System.ComponentModel;
using OntologySync_Module.Notifications;
using OntologySync_Module.Converter;

namespace OntologySync_Module
{
    public partial class FrmOntologySync : Form
    {


        private OntologySyncViewModelController viewModelController;
        private frmAuthenticate frmAuthenticate;

        
   

        public FrmOntologySync()
        {
            InitializeComponent();

            viewModelController = new OntologySyncViewModelController();
            viewModelController.SecurityWork = new clsSecurityWork(viewModelController.LocalConfig.Globals, this);
            viewModelController.PropertyChanged += ViewModelController_PropertyChanged;
            viewModelController.closeForm += ViewModelController_closeForm;
            viewModelController.getUser += ViewModelController_getUser;

            Initialize();
        }

        private void ViewModelController_closeForm(int exitCode)
        {
            if (this.InvokeRequired)
            {
                var deleg = new CloseForm(ViewModelController_closeForm);
                this.Invoke(deleg, exitCode);
            }
            else
            {
                this.Close();
            }
        }

        private void ViewModelController_getUser()
        {
            if (this.InvokeRequired)
            {
                var deleg = new GetUser(ViewModelController_getUser);
                this.Invoke(deleg);
            }
            else
            {
                frmAuthenticate = new frmAuthenticate(viewModelController.LocalConfig.Globals, true, false, frmAuthenticate.ERelateMode.NoRelate, true);

                if (frmAuthenticate.ShowDialog(this) == DialogResult.OK)
                {
                    viewModelController.UserItem = frmAuthenticate.OItem_User;
                }
                else
                {
                    viewModelController.CloseFormSuccessful();
                }
                
            }
        }

        private void ViewModelController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (this.InvokeRequired)
            {
                var deleg = new PropertyChangedEventHandler(ViewModelController_PropertyChanged);
                this.Invoke(deleg, sender, e);
            }
            else
            {
                if (e.PropertyName == NotifyChanges.OntologySync_SyncLogs)
                {
                    if (viewModelController.SyncLogs != null)
                    {
                        dataGridView_Sync.DataSource = new SortableBindingList<SyncLog>(viewModelController.SyncLogs);
                    }
                    
                }
                else if (e.PropertyName == NotifyChanges.OntologySync_IsEnabled_StopThread)
                {
                    toolStripButton_StopThread.Enabled = viewModelController.IsEnabled_StopThread;
                }
                else if (e.PropertyName == NotifyChanges.OntologySync_IsVisible_StopThread)
                {
                    toolStripButton_StopThread.Visible = viewModelController.IsVisible_StopThread;
                }
                else if (e.PropertyName == NotifyChanges.OntologySync_Text_StopThread)
                {
                    toolStripButton_StopThread.Text = viewModelController.Text_StopThread;
                }
                else if (e.PropertyName == NotifyChanges.OntologySync_IsVisible_TextStopThread)
                {
                    toolStripButton_StopThread.DisplayStyle = BoolToDisplayStyleConverter.Convert(viewModelController.IsVisible_TextStopThread);
                }
                else if (e.PropertyName == NotifyChanges.OntologySync_IsVisible_LoadProgress)
                {
                    toolStripProgressBar_Sync.Visible = viewModelController.IsVisible_LoadProgress;
                }
                else if (e.PropertyName == NotifyChanges.OntologySync_MinLoadProgress)
                {
                    toolStripProgressBar_Sync.Minimum = viewModelController.MaxLoadProgress;
                }
                else if (e.PropertyName == NotifyChanges.OntologySync_MaxLoadProgress)
                {
                    toolStripProgressBar_Sync.Minimum = viewModelController.MaxLoadProgress;
                }
                else if (e.PropertyName == NotifyChanges.OntologySync_ValueLoadProgress)
                {
                    toolStripProgressBar_Sync.Minimum = viewModelController.ValueLoadProgress;
                }
                else if (e.PropertyName == NotifyChanges.OntologySync_SyncLogToAdd)
                {
                    dataGridView_Sync.Refresh();
                }
               
            }
        }

        private void Initialize()
        {
            viewModelController.InitializeViewModel();
        }

        private void toolStripButton_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButton_Stop_Click(object sender, EventArgs e)
        {
            viewModelController.StopThread();
        }

        private void FrmOntologySync_FormClosing(object sender, FormClosingEventArgs e)
        {
            viewModelController.FinalizeViewModel();
        }
    }
}
