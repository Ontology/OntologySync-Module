﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OntologySync_Module.Converter
{
    public static class BoolToDisplayStyleConverter
    {
        public static ToolStripItemDisplayStyle Convert(bool isVisible)
        {
            if (isVisible)
            {
                return ToolStripItemDisplayStyle.ImageAndText;
            }
            else
            {
                return ToolStripItemDisplayStyle.Image;
            }
        }
    }
}
