﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OntologySync_Module.Converter
{
    public static class FormMessageToIconConverter
    {
        public static MessageBoxIcon Convert(FormMessage formMessage)
        {
            if (formMessage.FormMessageType.HasFlag(FormMessageType.Exclamation))
            {
                return MessageBoxIcon.Exclamation;
            }

            return MessageBoxIcon.Information;
        }
    }
}
