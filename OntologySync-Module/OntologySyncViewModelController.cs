﻿using Ontology_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologySync_Module.MessagingService;
using OntologySync_Module.Notifications;
using OntoMsg_Module;
using Security_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebSocketSharp;

namespace OntologySync_Module
{
    public delegate void GetUser();
    public delegate void CloseForm(int exitCode);


    public class OntologySyncViewModelController : OntologySyncViewModel
    {
        private Thread threadRefresh;
        private bool stopThread;

        public event GetUser getUser;
        public event CloseForm closeForm;

        private List<JobItem> jobItems;
        private List<WebConnection> webConnections;

        private clsLocalConfig localConfig;
        public clsLocalConfig LocalConfig
        {
            get { return localConfig; }
            set
            {
                localConfig = value;
            }
        }
        private ClsDataWorkOntologySync dataWorkOntologySync;

        private List<UserAuthentication> userAuthentications;

        private clsArgumentParsing objArgumentParsing;

        private clsExport exportWork;

        private MessageService messageServiceData;
        private MessageService messageServiceViewModel;

        private int max = 0;
        private int value = 0;


        public OntologySyncViewModelController()
        {
            PropertyChanged += OntologySyncViewModelController_PropertyChanged;
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }
            

        }

        private void OntologySyncViewModelController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == NotifyChanges.OntologySync_UserItem)
            {
                localConfig.OItemUser = UserItem;

                if (SecurityWork.initialize_User(localConfig.OItemUser).GUID == localConfig.Globals.LState_Success.GUID)
                {
                    dataWorkOntologySync = new ClsDataWorkOntologySync(localConfig);
                    dataWorkOntologySync.OItem_Configuration = localConfig.OItem_BaseConfig;
                    if (objArgumentParsing.OList_Items.Any())
                    {
                        if (objArgumentParsing.OList_Items.First().GUID_Parent ==
                            localConfig.OItem_object_baseconfig.GUID_Parent)
                        {
                            dataWorkOntologySync.OItem_Configuration = objArgumentParsing.OList_Items.First();
                        }
                    }

                    dataWorkOntologySync.loadItems += DataWorkOntologySync_loadItems;
                    var result = dataWorkOntologySync.GetData();
                }
                
            }
            else if (e.PropertyName == NotifyChanges.OntologySync_IsEnabled_StopThread)
            {
                var viewModel = new OntologySyncViewModel();
                viewModel.IsEnabled_StopThread = IsEnabled_StopThread;
                messageServiceViewModel.SendViewModel(viewModel, e.PropertyName);

            }
            else if (e.PropertyName == NotifyChanges.OntologySync_IsVisible_StopThread)
            {
                if (messageServiceViewModel == null) return;
                var viewModel = new OntologySyncViewModel();
                viewModel.IsVisible_StopThread = IsVisible_StopThread;
                messageServiceViewModel.SendViewModel(viewModel, e.PropertyName);

            }
            else if (e.PropertyName == NotifyChanges.OntologySync_Text_StopThread)
            {
                if (messageServiceViewModel == null) return;
                var viewModel = new OntologySyncViewModel();
                viewModel.Text_StopThread = Text_StopThread;
                messageServiceViewModel.SendViewModel(viewModel, e.PropertyName);

            }
            else if (e.PropertyName == NotifyChanges.OntologySync_IsVisible_TextStopThread)
            {
                if (messageServiceViewModel == null) return;
                var viewModel = new OntologySyncViewModel();
                viewModel.IsVisible_TextStopThread = IsVisible_TextStopThread;
                messageServiceViewModel.SendViewModel(viewModel, e.PropertyName);

            }
            else if (e.PropertyName == NotifyChanges.OntologySync_IsVisible_LoadProgress)
            {
                if (messageServiceViewModel == null) return;
                var viewModel = new OntologySyncViewModel();
                viewModel.IsVisible_LoadProgress = IsVisible_LoadProgress;
                messageServiceViewModel.SendViewModel(viewModel, e.PropertyName);

            }
            else if (e.PropertyName == NotifyChanges.OntologySync_MinLoadProgress)
            {
                if (messageServiceViewModel == null) return;
                var viewModel = new OntologySyncViewModel();
                viewModel.MinLoadProgress = MaxLoadProgress;
                messageServiceViewModel.SendViewModel(viewModel, e.PropertyName);

            }
            else if (e.PropertyName == NotifyChanges.OntologySync_MaxLoadProgress)
            {
                if (messageServiceViewModel == null) return;
                var viewModel = new OntologySyncViewModel();
                max += MaxLoadProgress;
                viewModel.MaxLoadProgress = max;
                messageServiceViewModel.SendViewModel(viewModel, e.PropertyName);

            }
            else if (e.PropertyName == NotifyChanges.OntologySync_ValueLoadProgress)
            {
                if (messageServiceViewModel == null) return;
                var viewModel = new OntologySyncViewModel();
                value += ValueLoadProgress;
                viewModel.ValueLoadProgress += value;
                messageServiceViewModel.SendViewModel(viewModel, e.PropertyName);

            }
            else if (e.PropertyName == NotifyChanges.OntologySync_SyncLogToAdd)
            {
                if (messageServiceViewModel == null) return;
                var viewModel = new OntologySyncViewModel();
                viewModel.SyncLogToAdd = SyncLogToAdd;
                messageServiceViewModel.SendViewModel(viewModel, e.PropertyName);
                IsVisible_LoadProgress = true;
                MinLoadProgress = 0;
                MaxLoadProgress = (int)viewModel.SyncLogToAdd.CountToDo;
                ValueLoadProgress = (int)viewModel.SyncLogToAdd.CountDone;
            }
        }

        private void DataWorkOntologySync_loadItems(LoadResult loadResult, clsOntologyItem oItemResult)
        {
            if (loadResult == LoadResult.WebConnection)
            {
                jobItems = dataWorkOntologySync.JobItems;

            }
            else if (loadResult == LoadResult.WebConnectionRel)
            {
                webConnections = dataWorkOntologySync.WebConnections;
            }
            else if (loadResult == LoadResult.AllOntologies)
            {
                AllOntologies = dataWorkOntologySync.AllOntologies;
            }
            else if (loadResult == LoadResult.Ontologies)
            {
                Ontologies = dataWorkOntologySync.BelongingOntologies;
            }
            else if (loadResult == LoadResult.Direction)
            {
                try
                {
                    threadRefresh.Abort();
                }
                catch(Exception ex)
                {

                }
                Direction = dataWorkOntologySync.Direction;
                if (Direction.GUID == localConfig.OItem_object_direction_export.GUID)
                {
                    threadRefresh = new Thread(ExportOntologies);
                }
                else
                {

                }
            }
            else if (loadResult == LoadResult.UserAuthRel)
            {
                userAuthentications = dataWorkOntologySync.UserAuthentications;
                var activeJobs = GetActiveJobs();
                var jobConnections = (from activeJob in activeJobs
                                      join webConnection in webConnections on activeJob.IdWebConnection equals
                                          webConnection.IdWebConnection
                                      join userAuth in userAuthentications on webConnection.IdUserAuthenatication equals
                                          userAuth.IdUserAuthentication
                                      select new
                                      {
                                          activeJob,
                                          webConnection,
                                          userAuth

                                      }).ToList();

                if (jobConnections.Any())
                {

                    if (Uri.IsWellFormedUriString(jobConnections.First().webConnection.NameUrl, UriKind.Absolute) &&
                        !string.IsNullOrEmpty(jobConnections.First().userAuth.NameUser) &&
                        !string.IsNullOrEmpty(jobConnections.First().userAuth.NamePassword))
                    {
                        var password = SecurityWork.decode_Password(jobConnections.First().userAuth.NamePassword);
                        messageServiceData = new MessageService(jobConnections.First().webConnection.NameUrl, localConfig);
                        messageServiceData.PropertyChanged += MessageService_PropertyChanged;
                        messageServiceViewModel = new MessageService("ws://176.28.40.26:6401/ViewModel", localConfig);
                        messageServiceData.PropertyChanged += MessageServiceData_PropertyChanged;

                        stopThread = false;
                        if (threadRefresh != null)
                        {
                            threadRefresh.Start();
                        }



                    }
                }
            }
            else
            {

            }
        }

        private void MessageServiceData_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == NotifyChanges.OntologyMessage_MessageState)
            {
                GetItemsRefresh(messageServiceData.Message.Ontology,
                        messageServiceData.Message.MessageType.ToString(),
                        messageServiceData.Message.MessageState.ToString(),
                        messageServiceData.Message.ItemCount,
                        0,
                        0,
                        0,
                        messageServiceData.IsSending ? localConfig.OItem_object_direction_import.Name : localConfig.OItem_object_direction_export.Name);
            }
        }

        private void MessageService_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == NotifyChanges.OntologyMessage_MessageState)
            {

                if (messageServiceData.Message.MessageState.HasFlag(MessageState.WorkingFinished))
                { 
                    GetItemsRefresh(messageServiceData.Message.Ontology,
                        messageServiceData.Message.MessageType.ToString(),
                        messageServiceData.Message.MessageState.ToString(),
                        messageServiceData.Message.ItemCount,
                        messageServiceData.Message.ItemCount,
                        0,
                        0,
                        messageServiceData.IsSending ? localConfig.OItem_object_direction_import.Name : localConfig.OItem_object_direction_export.Name);
                }
                else
                {
                    GetItemsRefresh(messageServiceData.Message.Ontology,
                        messageServiceData.Message.MessageType.ToString(),
                        messageServiceData.Message.MessageState.ToString(),
                        messageServiceData.Message.ItemCount,
                        0,
                        0,
                        0,
                        messageServiceData.IsSending ? localConfig.OItem_object_direction_import.Name : localConfig.OItem_object_direction_export.Name);
                }
                
            }
            
        }

        private List<JobItem> GetActiveJobs()
        {
            return jobItems.Where(jobItem => jobItem.IsActive).ToList();
        }

        private void ExportOntologies()
        {
            IsEnabled_StopThread = true;
            if (Ontologies.Any())
            {
                var webServiceAttributeTypes = new List<clsOntologyItem>();
                var webServiceRelationTypes = new List<clsOntologyItem>();
                var webServiceClasses = new List<clsOntologyItem>();
                var webServiceObjects = new List<clsOntologyItem>();
                var webServiceClassAtts = new List<clsClassAtt>();
                var webServiceClassRels = new List<clsClassRel>();
                var webServiceObjectAtts = new List<clsObjectAtt>();
                var webServiceObjectRels = new List<clsObjectRel>();

                foreach (var ontology in Ontologies)
                {
                    if (stopThread) break;
                    var result = exportWork.Generate_OntologyItems(ontology, ModeEnum.AllRelations);
                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        if (stopThread) break;
                        webServiceAttributeTypes.AddRange(from attTypeNew in
                           exportWork.OList_AttributeTypes.Select(attType => new clsOntologyItem
                           {
                               GUID = attType.GUID,
                               Name = attType.Name,
                               GUID_Parent = attType.GUID_Parent,
                               Type = attType.Type
                           })
                                                          join attTypeOld in webServiceAttributeTypes on attTypeNew.GUID equals attTypeOld.GUID into attTypesOld
                                                          from attTypeOld in attTypesOld.DefaultIfEmpty()
                                                          where attTypeOld == null
                                                          select attTypeNew);

                        GetItemsRefresh(ontology.Name,
                            localConfig.Globals.Type_AttributeType,
                            "Get Items",
                            webServiceAttributeTypes.Count,
                            0,
                            0,
                            0,
                            localConfig.OItem_object_direction_export.Name);

                        if (stopThread) break;
                        webServiceClasses.AddRange(from classNew in
                            exportWork.OList_Classes.Select(clsType => new clsOntologyItem
                            {
                                GUID = clsType.GUID,
                                Name = clsType.Name,
                                GUID_Parent = clsType.GUID_Parent,
                                Type = clsType.Type
                            })
                                                   join classOld in webServiceClasses on classNew.GUID equals classOld.GUID into classesOld
                                                   from classOld in classesOld.DefaultIfEmpty()
                                                   where classOld == null
                                                   select classNew);

                        GetItemsRefresh(ontology.Name,
                            localConfig.Globals.Type_Class,
                            "Get Items",
                            webServiceClasses.Count,
                            0,
                            0,
                            0,
                            localConfig.OItem_object_direction_export.Name);

                        if (stopThread) break;
                        webServiceRelationTypes.AddRange(from relNew in
                            exportWork.OList_RelationTypes.Select(relTypes => new clsOntologyItem
                            {
                                GUID = relTypes.GUID,
                                Name = relTypes.Name,
                                GUID_Parent = relTypes.GUID_Parent,
                                Type = relTypes.Type
                            })
                                                         join relOld in webServiceRelationTypes on relNew.GUID equals relOld.GUID into relsOld
                                                         from relOld in relsOld.DefaultIfEmpty()
                                                         where relOld == null
                                                         select relNew);

                        GetItemsRefresh(ontology.Name,
                            localConfig.Globals.Type_RelationType,
                            "Get Items",
                            webServiceRelationTypes.Count,
                            0,
                            0,
                            0,
                            localConfig.OItem_object_direction_export.Name);

                        if (stopThread) break;
                        webServiceObjects.AddRange(from objNew in
                            exportWork.OList_Objects.Select(objTypes => new clsOntologyItem
                            {
                                GUID = objTypes.GUID,
                                Name = objTypes.Name,
                                GUID_Parent = objTypes.GUID_Parent,
                                Type = objTypes.Type
                            })
                                                   join objOld in webServiceObjects on objNew.GUID equals objOld.GUID into objsOld
                                                   from objOld in objsOld.DefaultIfEmpty()
                                                   where objOld == null
                                                   select objNew);
                        GetItemsRefresh(ontology.Name,
                            localConfig.Globals.Type_Object,
                            "Get Items",
                            webServiceObjects.Count,
                            0,
                            0,
                            0,
                            localConfig.OItem_object_direction_export.Name);

                        if (stopThread) break;
                        webServiceClassAtts.AddRange(from clsAttNew in
                            exportWork.OList_ClassAtts.Select(clsAtt => new clsClassAtt
                            {
                                ID_AttributeType = clsAtt.ID_AttributeType,
                                ID_Class = clsAtt.ID_Class,
                                ID_DataType = clsAtt.ID_DataType,
                                Min = clsAtt.Min,
                                Max = clsAtt.Max
                            })
                                                     join clsAttOld in webServiceClassAtts on new { ID_Class = clsAttNew.ID_Class, ID_AttributeType = clsAttNew.ID_AttributeType } equals new { ID_Class = clsAttOld.ID_Class, ID_AttributeType = clsAttOld.ID_AttributeType } into objClassAttsOld
                                                     from clsAttOld in objClassAttsOld.DefaultIfEmpty()
                                                     where clsAttOld == null
                                                     select clsAttNew);
                        GetItemsRefresh(ontology.Name,
                            localConfig.Globals.Type_ClassAtt,
                            "Get Items",
                            webServiceClassAtts.Count,
                            0,
                            0,
                            0,
                            localConfig.OItem_object_direction_export.Name);

                        if (stopThread) break;
                        webServiceClassRels.AddRange(from classRelNew in
                            exportWork.OList_ClassRel.Select(clsRel => new clsClassRel
                            {
                                ID_Class_Left = clsRel.ID_Class_Left,
                                ID_Class_Right = clsRel.ID_Class_Right,
                                ID_RelationType = clsRel.ID_RelationType,
                                Ontology = clsRel.Ontology,
                                Min_Forw = clsRel.Min_Forw,
                                Max_Forw = clsRel.Max_Forw,
                                Max_Backw = clsRel.Max_Backw
                            })
                                                     join classRelOld in webServiceClassRels on new
                                                     {
                                                         ID_Class_Left = classRelNew.ID_Class_Left,
                                                         ID_Class_Right = classRelNew.ID_Class_Right ?? "1",
                                                         ID_RelationType = classRelNew.ID_RelationType
                                                     } equals new
                                                     {
                                                         ID_Class_Left = classRelOld.ID_Class_Left,
                                                         ID_Class_Right = classRelOld.ID_Class_Right ?? "1",
                                                         ID_RelationType = classRelOld.ID_RelationType
                                                     } into classRelsOld
                                                     from classRelOld in classRelsOld.DefaultIfEmpty()
                                                     where classRelOld == null
                                                     select classRelNew);
                        GetItemsRefresh(ontology.Name,
                            localConfig.Globals.Type_ClassRel,
                            "Get Items",
                            webServiceClassRels.Count,
                            0,
                            0,
                            0,
                            localConfig.OItem_object_direction_export.Name);

                        if (stopThread) break;
                        webServiceObjectAtts.AddRange(from objAttNew in
                            exportWork.OList_ObjectAtt.Select(objAtt => new clsObjectAtt
                            {
                                ID_Attribute = objAtt.ID_Attribute,
                                ID_AttributeType = objAtt.ID_AttributeType,
                                ID_Class = objAtt.ID_Class,
                                ID_Object = objAtt.ID_Object,
                                ID_DataType = objAtt.ID_DataType,
                                OrderID = objAtt.OrderID,
                                Val_Bit = objAtt.Val_Bit,
                                Val_Date = objAtt.Val_Date,
                                Val_Double = objAtt.Val_Double,
                                Val_Int = objAtt.Val_Int,
                                Val_String = objAtt.Val_String,
                                Val_Name = objAtt.Val_Name
                            })
                                                      join objAttOld in webServiceObjectAtts on objAttNew.ID_Attribute equals objAttOld.ID_Attribute into objAttsOld
                                                      from objAttOld in objAttsOld.DefaultIfEmpty()
                                                      where objAttOld == null
                                                      select objAttNew);
                        GetItemsRefresh(ontology.Name,
                            localConfig.Globals.Type_ObjectAtt,
                            "Get Items",
                            webServiceObjectAtts.Count,
                            0,
                            0,
                            0,
                            localConfig.OItem_object_direction_export.Name);

                        if (stopThread) break;
                        webServiceObjectRels.AddRange(from objRelNew in
                            exportWork.OList_ObjectRel.Select(objRel => new clsObjectRel
                            {
                                ID_Object = objRel.ID_Object,
                                ID_Parent_Object = objRel.ID_Parent_Object,
                                ID_Other = objRel.ID_Other,
                                ID_Parent_Other = objRel.ID_Parent_Other,
                                ID_RelationType = objRel.ID_RelationType,
                                OrderID = objRel.OrderID,
                                Ontology = objRel.Ontology
                            })
                                                      join objRelOld in webServiceObjectRels on new
                                                      {
                                                          ID_Object = objRelNew.ID_Object,
                                                          ID_Parent_Object = objRelNew.ID_Parent_Object,
                                                          ID_Other = objRelNew.ID_Other,
                                                          ID_Parent_Other = objRelNew.ID_Parent_Other ?? "1",
                                                          ID_RelationType = objRelNew.ID_RelationType
                                                      } equals new
                                                      {
                                                          ID_Object = objRelOld.ID_Object,
                                                          ID_Parent_Object = objRelOld.ID_Parent_Object,
                                                          ID_Other = objRelOld.ID_Other,
                                                          ID_Parent_Other = objRelOld.ID_Parent_Other ?? "",
                                                          ID_RelationType = objRelOld.ID_RelationType
                                                      } into objRelsOld
                                                      from objRelOld in objRelsOld.DefaultIfEmpty()
                                                      where objRelOld == null
                                                      select objRelNew);
                        GetItemsRefresh(ontology.Name,
                            localConfig.Globals.Type_ObjectRel,
                            "Get Items",
                            webServiceObjectRels.Count,
                            0,
                            0,
                            0,
                            localConfig.OItem_object_direction_export.Name);

                        if (stopThread) break;
                        if (webServiceAttributeTypes.Any())
                        {
                            clsOntologyItem webResult;
                            if (webServiceAttributeTypes.Count >= 1000)
                            {
                                int start = 0;
                                int count = 0;
                                while (start < webServiceAttributeTypes.Count)
                                {
                                    count = webServiceAttributeTypes.Count - start;
                                    count = count > 1000 ? 1000 : count;
                                    messageServiceData.SendAttributeTypes(webServiceAttributeTypes.GetRange(start, count), ontology.Name);
                                    
                                    start += 1000;
                                }
                            }
                            else
                            {
                                messageServiceData.SendAttributeTypes(webServiceAttributeTypes, ontology.Name);

                            }

                            webServiceAttributeTypes.Clear();
                        }

                        if (stopThread) break;
                        if (webServiceClasses.Any())
                        {
                            if (webServiceClasses.Count >= 1000)
                            {
                                int start = 0;
                                int count = 0;
                                while (start < webServiceClasses.Count)
                                {
                                    count = webServiceClasses.Count - start;
                                    count = count > 1000 ? 1000 : count;

                                    messageServiceData.SendClasses(webServiceClasses.GetRange(start, count), ontology.Name);
                                    
                                    start += 1000;
                                }
                            }
                            else
                            {
                                messageServiceData.SendClasses(webServiceClasses, ontology.Name);
                                

                            }

                            webServiceClasses.Clear();
                        }

                        if (stopThread) break;
                        if (webServiceRelationTypes.Any())
                        {
                            if (webServiceRelationTypes.Count >= 1000)
                            {
                                int start = 0;
                                int count = 0;
                                while (start < webServiceRelationTypes.Count)
                                {
                                    count = webServiceRelationTypes.Count - start;
                                    count = count > 1000 ? 1000 : count;
                                    messageServiceData.SendRelationTypes(webServiceRelationTypes.GetRange(start, count), ontology.Name);
                                    
                                    start += 1000;
                                }
                            }
                            else
                            {
                                messageServiceData.SendRelationTypes(webServiceRelationTypes, ontology.Name);
                                
                            }

                            webServiceRelationTypes.Clear();
                        }

                        if (stopThread) break;
                        if (webServiceObjects.Any())
                        {
                            if (webServiceObjects.Count >= 1000)
                            {
                                int start = 0;
                                int count = 0;
                                while (start < webServiceObjects.Count)
                                {
                                    count = webServiceObjects.Count - start;
                                    count = count > 1000 ? 1000 : count;
                                    messageServiceData.SendObjects(webServiceObjects.GetRange(start, count), ontology.Name);
                                    
                                    start += 1000;
                                }

                            }
                            else
                            {
                                messageServiceData.SendObjects(webServiceObjects, ontology.Name);
                                
                            }
                            webServiceObjects.Clear();

                        }

                        if (stopThread) break;
                        if (webServiceClassAtts.Any())
                        {
                            if (webServiceClassAtts.Count >= 1000)
                            {
                                int start = 0;
                                int count = 0;
                                while (start < webServiceClassAtts.Count)
                                {
                                    count = webServiceClassAtts.Count - start;
                                    count = count > 1000 ? 1000 : count;
                                    messageServiceData.SendClassAttributes(webServiceClassAtts.GetRange(start, count), ontology.Name);
                                    
                                    start += 1000;
                                }
                            }
                            else
                            {
                                messageServiceData.SendClassAttributes(webServiceClassAtts, ontology.Name);
                                
                            }

                            webServiceClassAtts.Clear();
                        }

                        if (stopThread) break;
                        if (webServiceClassRels.Any())
                        {
                            if (webServiceClassRels.Count >= 1000)
                            {
                                int start = 0;
                                int count = 0;
                                while (start < webServiceClassRels.Count)
                                {
                                    count = webServiceClassRels.Count - start;
                                    count = count > 1000 ? 1000 : count;

                                    messageServiceData.SendClassRelations(webServiceClassRels.GetRange(start,count), ontology.Name);
                                    
                                    start += 1000;
                                }
                            }
                            else
                            {
                                messageServiceData.SendClassRelations(webServiceClassRels, ontology.Name);
                                
                            }

                            webServiceClassRels.Clear();
                        }

                        if (stopThread) break;
                        if (webServiceObjectAtts.Any())
                        {
                            if (webServiceObjectAtts.Count >= 1000)
                            {
                                int start = 0;
                                int count = 0;
                                while (start < webServiceObjectAtts.Count)
                                {
                                    count = webServiceObjectAtts.Count - start;
                                    count = count > 1000 ? 1000 : count;
                                    messageServiceData.SendObjectAttributes(webServiceObjectAtts.GetRange(start, count), ontology.Name);
                                    
                                    start += 1000;
                                }
                            }
                            else
                            {
                                messageServiceData.SendObjectAttributes(webServiceObjectAtts, ontology.Name);
                                
                            }

                            webServiceObjectAtts.Clear();
                        }

                        if (stopThread) break;
                        if (webServiceObjectRels.Any())
                        {
                            if (webServiceObjectRels.Count >= 1000)
                            {
                                int start = 0;
                                int count = 0;
                                while (start < webServiceObjectRels.Count)
                                {
                                    count = webServiceObjectRels.Count - start;
                                    count = count > 1000 ? 1000 : count;

                                    messageServiceData.SendObjectAttributes(webServiceObjectAtts.GetRange(start, count), ontology.Name);
                                    
                                    start += 1000;
                                }
                            }
                            else
                            {
                                messageServiceData.SendObjectAttributes(webServiceObjectAtts, ontology.Name);
                                
                            }

                            webServiceObjectRels.Clear();
                        }

                    }
                }


            }
            else
            {
                IsEnabled_StopThread = false;
            }
        }

        public void InitializeViewModel()
        {
            IsEnabled_StopThread = false;
            IsVisible_StopThread = true;
            IsVisible_TextStopThread = false;
            Text_StopThread = "Stop";
            IsVisible_LoadProgress = false;
            MinLoadProgress = 0;
            MaxLoadProgress = 100;
            ValueLoadProgress = 0;

            
            objArgumentParsing = new clsArgumentParsing(localConfig.Globals, Environment.GetCommandLineArgs().ToList());
            SyncLogs = new List<SyncLog>();
            exportWork = new clsExport(localConfig.Globals);
            if (getUser != null)
            {
                getUser();
            }
        }

        public void CloseFormSuccessful()
        {
            CloseForm(0);
        }

        public void CloseFormFailure()
        {
            CloseForm(-1);
        }

        private void CloseForm(int exitCode = 0)
        {
            if (closeForm != null)
            {
                closeForm(exitCode);
            }
        }

        public void StopThread()
        {
            stopThread = true;
        }

        private void GetItemsRefresh(string ontology, string type, string step, long countToDo, long countDone, long countNothingToDo, long countError, string direction)
        {
            var syncLogToAdd = new SyncLog
            {
                Ontology = ontology,
                Type = type,
                Step = step,
                CountToDo = countToDo,
                CountDone = countDone,
                CountNothingToDo = countNothingToDo,
                CountError = countError,
                Direction = direction
            };
            SyncLogs.Add(SyncLogToAdd);
            SyncLogToAdd = syncLogToAdd;
        }

        public void FinalizeViewModel()
        {
            
        }
    }

    
}
