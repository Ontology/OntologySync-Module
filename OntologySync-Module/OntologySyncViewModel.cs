﻿using OntologyClasses.BaseClasses;
using OntologySync_Module.Notifications;
using OntologySync_Module.ViewModels;
using Security_Module;
using Structure_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologySync_Module
{
    public class OntologySyncViewModel : ViewModelBase
    {
        private List<SyncLog> syncLogs;
        public List<SyncLog> SyncLogs
        {
            get { return syncLogs; }
            set
            {
                syncLogs = value;
                RaisePropertyChanged(NotifyChanges.OntologySync_SyncLogs);
            }
        }

        private clsOntologyItem userItem;
        public clsOntologyItem UserItem
        {
            get { return userItem; }
            set
            {
                if (userItem == value) return;

                userItem = value;
                RaisePropertyChanged(NotifyChanges.OntologySync_UserItem);
            }
        }

        private clsSecurityWork securityWork;
        public clsSecurityWork SecurityWork
        {
            get { return securityWork; }
            set
            {
                if (securityWork == value) return;
                securityWork = value;
                RaisePropertyChanged(NotifyChanges.OntologySync_SecurityWork);
            }
        }

        private bool allOntologies;
        public bool AllOntologies
        {
            get { return allOntologies; }
            set
            {
                if (allOntologies == value) return;
                allOntologies = value;
                RaisePropertyChanged(NotifyChanges.OntologySync_AllOntologies);
            }
        }

        private List<clsOntologyItem> ontologies;
        public List<clsOntologyItem> Ontologies
        {
            get { return ontologies; }
            set
            {
                if (ontologies == value) return;
                ontologies = value;
                RaisePropertyChanged(NotifyChanges.OntologySync_Ontologies);

            }
        }

        private clsOntologyItem direction;
        public clsOntologyItem Direction
        {
            get { return direction; }
            set
            {
                if (direction == value) return;
                direction = value;
                RaisePropertyChanged(NotifyChanges.OntologySync_Direction);
            }
        }

        private bool isEnabled_StopThread;
        public bool IsEnabled_StopThread
        {
            get { return isEnabled_StopThread; }
            set
            {
                if (isEnabled_StopThread == value) return;
                isEnabled_StopThread = value;
                RaisePropertyChanged(NotifyChanges.OntologySync_IsEnabled_StopThread);
            }
        }

        private bool isVisible_StopThread;
        public bool IsVisible_StopThread
        {
            get { return isVisible_StopThread; }
            set
            {
                if (isVisible_StopThread == value) return;
                isVisible_StopThread = value;
                RaisePropertyChanged(NotifyChanges.OntologySync_IsVisible_StopThread);
            }
        }

        private bool isVisible_TextStopThread;
        public bool IsVisible_TextStopThread
        {
            get { return isVisible_TextStopThread; }
            set
            {
                if (isVisible_TextStopThread == value) return;
                isVisible_TextStopThread = value;
                RaisePropertyChanged(NotifyChanges.OntologySync_IsVisible_TextStopThread);
            }
        }

        private string text_StopThread;
        public string Text_StopThread
        {
            get { return text_StopThread; }
            set
            {
                if (text_StopThread == value) return;
                text_StopThread = value;
                RaisePropertyChanged(NotifyChanges.OntologySync_Text_StopThread);
            }
        }


        private bool isEnabled_LoadProgress;
        public bool IsEnabled_LoadProgress
        {
            get { return isEnabled_LoadProgress; }
            set
            {
                if (isEnabled_LoadProgress == value) return;
                isEnabled_LoadProgress = value;
                RaisePropertyChanged(NotifyChanges.OntologySync_IsEnabled_LoadProgress);
            }
        }

        private bool isVisible_LoadProgress;
        public bool IsVisible_LoadProgress
        {
            get { return isVisible_LoadProgress; }
            set
            {
                if (isVisible_LoadProgress == value) return;
                isVisible_LoadProgress = value;
                RaisePropertyChanged(NotifyChanges.OntologySync_IsVisible_LoadProgress);
            }
        }


        private int minLoadProgress;
        public int MinLoadProgress
        {
            get { return minLoadProgress; }
            set
            {
                minLoadProgress = value;
                RaisePropertyChanged(NotifyChanges.OntologySync_MinLoadProgress);
            }
        }

        private int maxLoadProgress;
        public int MaxLoadProgress
        {
            get { return maxLoadProgress; }
            set
            {
                maxLoadProgress = value;
                RaisePropertyChanged(NotifyChanges.OntologySync_MaxLoadProgress);
            }
        }

        private int valueLoadProgress;
        public int ValueLoadProgress
        {
            get { return valueLoadProgress; }
            set
            {
                valueLoadProgress = value;
                RaisePropertyChanged(NotifyChanges.OntologySync_ValueLoadProgress);
            }
        }

        private SyncLog syncLogToAdd;
        public SyncLog SyncLogToAdd
        {
            get { return syncLogToAdd; }
            set
            {
                if (syncLogToAdd == value) return;
                syncLogToAdd = value;
                RaisePropertyChanged(NotifyChanges.OntologySync_SyncLogToAdd);
            }
        }
    }
}
