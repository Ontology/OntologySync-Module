﻿using Newtonsoft.Json;
using OntologyClasses.BaseClasses;
using OntologySync_Module.Notifications;
using OntologySync_Module.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;

namespace OntologySync_Module.MessagingService
{
    public class MessageService : NotifyPropertyChange
    {

        private clsLocalConfig localConfig;

        private WebSocket websocketService;
        public WebSocket WebSocketService
        {
            get { return websocketService; }
            private set
            {
                websocketService = value;
            }
        }
        private OntologyMessage message;

        public bool IsSending { get; private set; }

        public OntologyMessage Message
        {
            get { return message; }
            set
            {
                if (message == value) return;
                message = value;
                RaisePropertyChanged(NotifyChanges.MessageService_Message);
            }
        }
        public void SendAttributeTypes(List<clsOntologyItem> attributeTypes, string ontology)
        {
            PrepareSending(ontology);
            Message.AttributeTypes = attributeTypes;
            StringBuilder sb = new StringBuilder();

            sb.Append(JsonConvert.SerializeObject(attributeTypes));

            sendMessage(sb);
        }

        private void PrepareSending(string ontology)
        {
            IsSending = true;
            Message.Ontology = ontology;
            Message.ClearItems();
            Message.AddMessageState(MessageState.IsPreparing);
        }

        private void sendMessage(StringBuilder sb)
        {
            Message.AddMessageState(MessageState.IsPrepared);

            Message.AddMessageState(MessageState.IsWorking);
            websocketService.Send(sb.ToString());
            Message.AddMessageState(MessageState.WorkingFinished);
        }

        public void SendClasses(List<clsOntologyItem> classes, string ontology)
        {
            PrepareSending(ontology);
            Message.Classes = classes;
            StringBuilder sb = new StringBuilder();

            sb.Append(JsonConvert.SerializeObject(classes));

            sendMessage(sb);
        }

        public void SendRelationTypes(List<clsOntologyItem> relationTypes, string ontology)
        {
            PrepareSending(ontology);
            Message.RelationTypes = relationTypes;
            StringBuilder sb = new StringBuilder();

            sb.Append(JsonConvert.SerializeObject(relationTypes));

            sendMessage(sb);
        }

        public void SendObjects(List<clsOntologyItem> objects, string ontology)
        {
            PrepareSending(ontology);
            Message.Objects = objects;
            StringBuilder sb = new StringBuilder();

            sb.Append(JsonConvert.SerializeObject(objects));

            sendMessage(sb);
        }

        public void SendClassAttributes(List<clsClassAtt> classAttributes, string ontology)
        {
            PrepareSending(ontology);
            Message.ClassAttributes = classAttributes;
            StringBuilder sb = new StringBuilder();

            sb.Append(JsonConvert.SerializeObject(classAttributes));

            sendMessage(sb);
        }

        public void SendClassRelations(List<clsClassRel> classRelations, string ontology)
        {
            PrepareSending(ontology);
            Message.ClassRelations = classRelations;
            StringBuilder sb = new StringBuilder();

            sb.Append(JsonConvert.SerializeObject(classRelations));

            sendMessage(sb);
        }

        public void SendObjectAttributes(List<clsObjectAtt> objectAttributes, string ontology)
        {
            PrepareSending(ontology);
            Message.ObjectAttributes = objectAttributes;
            StringBuilder sb = new StringBuilder();

            sb.Append(JsonConvert.SerializeObject(objectAttributes));

            sendMessage(sb);
        }

        public void SendObjectRelations(List<clsObjectRel> objectRelations, string ontology)
        {
            PrepareSending(ontology);
            Message.ObjectRelations = objectRelations;
            StringBuilder sb = new StringBuilder();

            sb.Append(JsonConvert.SerializeObject(objectRelations));

            sendMessage(sb);
        }

        public void SendObjectTree(List<clsObjectTree> objectTree, string ontology)
        {
            PrepareSending(ontology);
            Message.ObjectTrees = objectTree;
            StringBuilder sb = new StringBuilder();

            sb.Append(JsonConvert.SerializeObject(objectTree));

            sendMessage(sb);
        }

        public MessageService(string uri, clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            websocketService = new WebSocket(uri);
            websocketService.Connect();
            Message = new OntologyMessage();
            Message.PropertyChanged += Message_PropertyChanged;
        }

        ~MessageService()
        {
            if (websocketService != null && websocketService.ReadyState != WebSocketState.Closed)
            {
                websocketService.Close();
            }
        }
        
        public void SendViewModel(OntologySyncViewModel viewModel, string propertyToRefresh)
        {
            Message = new OntologyMessage();
            IsSending = true;
            Message.Ontology = null;
            Message.ClearItems();
            Message.AddMessageState(MessageState.IsPreparing);

            Message.ModuleId = localConfig.IdLocalConfig;
            message.PropertyToRefresh = propertyToRefresh;
            Message.Model = viewModel;
            var sb = new StringBuilder();
            sb.Append(JsonConvert.SerializeObject(Message));
            sendMessage(sb);
        }

        private void Message_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            RaisePropertyChanged(e.PropertyName);
        }
    }
}
