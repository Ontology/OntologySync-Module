﻿using OntologyClasses.BaseClasses;
using OntologySync_Module.Notifications;
using OntologySync_Module.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologySync_Module.MessagingService
{
    
    [Flags]
    public enum MessageState
    {
        Idle = 0,
        IsPreparing = 2,
        IsPrepared = 4,
        IsWorking = 8,
        WorkingFinished = 16
    }

    [Flags]
    public enum MessageType
    {
        None = 0,
        AttributeTypes = 1,
        Classes = 2,
        Objects = 4,
        RelationTypes = 8,
        ClassAttributes = 16,
        ClassRelations = 32,
        ObjectAttributes = 64,
        ObjectRelations = 128,
        ObjectTrees = 256
    }

    public class OntologyMessage : NotifyPropertyChange
    {
        public MessageType MessageType
        {
            get
            {
                if (AttributeTypes != null)
                {
                    return MessageType.AttributeTypes;
                }
                else if (Classes != null)
                {
                    return MessageType.Classes;
                }
                else
                {
                    return MessageType.None;
                }
            }
        }

        public string Ontology { get; set; }
        public string ModuleId { get; set; }
        public string FunctionId { get; set; }

        public string PropertyToRefresh { get; set; }
        public OntologySyncViewModel Model { get; set; }
        
        private MessageState messageState;
        public MessageState MessageState
        {
            get { return messageState; }
            set
            {
                if (messageState == value) return;
                messageState = value;
                RaisePropertyChanged(NotifyChanges.OntologyMessage_MessageState);
            }
        }

        private List<clsOntologyItem> attributeTypes;
        public List<clsOntologyItem> AttributeTypes
        {
            get { return attributeTypes; }
            set
            {
                attributeTypes = value;
                Classes = null;
                RelationTypes = null;
                Objects = null;

            }
        }

        public List<clsOntologyItem> classes;
        public List<clsOntologyItem> Classes
        {
            get { return classes; }
            set
            {
                classes = value;
            }
        }

        public List<clsOntologyItem> objects;
        public List<clsOntologyItem> Objects
        {
            get { return objects; }
            set
            {
                objects = value;
            }
        }

        public List<clsOntologyItem> relationTypes;
        public List<clsOntologyItem> RelationTypes
        {
            get { return relationTypes; }
            set
            {
                relationTypes = value;
            }
        }

        public List<clsClassAtt> classAttributes;
        public List<clsClassAtt> ClassAttributes
        {
            get { return classAttributes; }
            set
            {
                classAttributes = value;
            }
        }

        public List<clsClassRel> classRelations;
        public List<clsClassRel> ClassRelations
        {
            get { return classRelations; }
            set
            {
                classRelations = value;
            }
        }

        public List<clsObjectAtt> objectAttributes;
        public List<clsObjectAtt> ObjectAttributes
        {
            get { return objectAttributes; }
            set
            {
                objectAttributes = value;
            }
        }

        public List<clsObjectRel> objectRelations;
        public List<clsObjectRel> ObjectRelations
        {
            get { return objectRelations; }
            set
            {
                objectRelations = value;
            }
        }

        public List<clsObjectTree> objectTrees;
        public List<clsObjectTree> ObjectTrees
        {
            get { return objectTrees; }
            set
            {
                objectTrees = value;
            }
        }

        public void AddMessageState(MessageState messageState)
        {
            MessageState |= messageState;
        }

        public void RemoveMessageState(MessageState messageState)
        {
            MessageState &= ~messageState;
        }

        public long ItemCount
        {
            get 
            {
                long count = 0;

                count += AttributeTypes != null ? AttributeTypes.Count : 0;
                count += RelationTypes != null ? RelationTypes.Count : 0;
                count += Classes != null ? Classes.Count : 0;
                count += Objects != null ? Objects.Count : 0;
                count += ClassAttributes != null ? ClassAttributes.Count : 0;
                count += ClassRelations != null ? ClassRelations.Count : 0;
                count += ObjectAttributes != null ? ObjectAttributes.Count : 0;
                count += ObjectRelations != null ? ObjectRelations.Count : 0;
                count += ObjectTrees != null ? ObjectTrees.Count : 0;

                return count;
            }
        }

        public void ClearItems()
        {
            AttributeTypes = null;
            Classes = null;
            RelationTypes = null;
            Objects = null;
            ClassAttributes = null;
            ClassRelations = null;
            ObjectAttributes = null;
            ObjectRelations = null;
            ObjectTrees = null;
            MessageState = MessageState.Idle;
        }

    }
}
